﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Serilog;
using WebChat.Application.DataContracts.Responses;
using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Shared.Exceptions;
using WebChat.Infrastructure.Services.Interfaces;

namespace WebChat.Api.Filters;

public sealed class ExceptionFilter : ExceptionFilterAttribute {
    public override async Task OnExceptionAsync(ExceptionContext context) {
        await RollbackTransactionIfExists(context);

        switch (context.Exception) {
            case ApiException ex:
                Log.Warning("{Url}: {Message}", context.HttpContext.Request.Path, ex.Message);
                context.Result = new ObjectResult(ApiResponse.Failed(new ApiError(ex.Message, ex.ErrorCode)));
                break;
            case { } ex:
                Log.Error(ex, "Untyped exception for: {Url}", context.HttpContext.Request.Path);
                context.Result = new ObjectResult(ApiResponse.Failed(new ApiError(ex.Message, ApiErrorCode.UnknownError)));
                break;
        }

        await base.OnExceptionAsync(context);
    }

    private static async Task RollbackTransactionIfExists(ActionContext context) {
        var transactionService = context.HttpContext.RequestServices.GetService<ITransactionService>();
        await transactionService?.TryRollbackAsync()!;
    }
}