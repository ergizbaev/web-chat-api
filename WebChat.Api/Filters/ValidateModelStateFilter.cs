﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using WebChat.Domain.Constants.Enum;

namespace WebChat.Api.Filters;

public sealed class ValidateModelStateFilter : ActionFilterAttribute {
    public override void OnActionExecuting(ActionExecutingContext context) {
        if (context.ModelState.IsValid)
            return;

        var errors = context.ModelState.SelectMany(x => x.Value?.Errors!).Select(x => x.ErrorMessage);
        var decoratedError = string.Join(Environment.NewLine, errors);
        context.Result = new JsonResult(new { Message = decoratedError, ErrorCode = ApiErrorCode.ValidationError });
    }
}