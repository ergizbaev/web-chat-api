﻿using System.Data;

namespace WebChat.Api.Attributes;

[AttributeUsage(AttributeTargets.Method)]
public class TransactionalAttribute : Attribute {

    public TransactionalAttribute(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted) {
        this.IsolationLevel = isolationLevel;
    }

    public IsolationLevel IsolationLevel { get; }
}