﻿using MassTransit;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Events;
using WebChat.Infrastructure.Databases.EntityFramework;
using WebChat.Infrastructure.SignalR.Hubs;

namespace WebChat.Api.Consumers;

public sealed class UsersInvitedToGroupEventConsumer : IConsumer<UsersInvitedToGroupEvent> {
    private readonly IHubContext<ChatHub> _chatHubContext;
    private readonly AppDbContext _dbContext;
    private readonly ILogger<UsersInvitedToGroupEventConsumer> _logger;

    public UsersInvitedToGroupEventConsumer(AppDbContext dbContext, IHubContext<ChatHub> chatHubContext, ILogger<UsersInvitedToGroupEventConsumer> logger) {
        this._dbContext = dbContext;
        this._chatHubContext = chatHubContext;
        this._logger = logger;
    }
    
    public async Task Consume(ConsumeContext<UsersInvitedToGroupEvent> context) {
        var data = context.Message;

        try {
            var connectionIds = await this._dbContext.Users
                .Where(x => data.UsersIds.Contains(x.Id) && x.Status != UserStatusEnum.Offline)
                .Select(x => x.ConnectionId)
                .ToArrayAsync(context.CancellationToken);

            await Parallel.ForEachAsync(connectionIds, async (connectionId, _) => {
                await this._chatHubContext.Groups.AddToGroupAsync(connectionId, $"GroupChat-{data.ChatId}", context.CancellationToken);
            });

            await this._chatHubContext.Clients
                .Group($"GroupChat-{data.ChatId}")
                .SendAsync("OnInvitedToGroupChat", $"Вы приглашены в чат с Id {data.ChatId}");
        } 
        catch (Exception ex) {
            this._logger.LogError(ex, $"Ошибка при приглашении в группу - {ex.Message}");
            await context.Redeliver(TimeSpan.FromSeconds(15));
        }
    }
}