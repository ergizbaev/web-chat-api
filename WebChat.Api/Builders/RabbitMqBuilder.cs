﻿using MassTransit;
using WebChat.Api.Consumers;
using WebChat.Domain.Constants;
using WebChat.Domain.Shared.Configs;

namespace WebChat.Api.Builders;

public static class RabbitMqBuilder {
    public static void AddRabbitMqAndMassTransit(this IServiceCollection services) {
        var rabbitMqConfig = services.BuildServiceProvider().GetRequiredService<RabbitMqConfig>();

        services.AddMassTransit(x => {
            x.SetKebabCaseEndpointNameFormatter();

            x.AddConsumer<UsersInvitedToGroupEventConsumer>();

            x.UsingRabbitMq((context, cfg) => {
                cfg.Host($"rabbitmq://{rabbitMqConfig.Url}/{rabbitMqConfig.VirtualHost}", configurator => {
                    configurator.Username(rabbitMqConfig.UserName);
                    configurator.Password(rabbitMqConfig.Password);
                });

                cfg.ReceiveEndpoint(QueueNames.InviteUsersToGroup, e => {
                    e.ConfigureConsumer<UsersInvitedToGroupEventConsumer>(context);
                });
            });
        });
    }
}