﻿using Microsoft.AspNetCore.Http.Features;
using WebChat.Api.Attributes;
using WebChat.Infrastructure.Services.Interfaces;

namespace WebChat.Api.Middlewares;

public class TransactionalMiddleware {
    private readonly RequestDelegate _next;

    public TransactionalMiddleware(RequestDelegate next) {
        this._next = next;
    }

    public async Task Invoke(HttpContext httpContext) {
        var endpoint = httpContext.Features.Get<IEndpointFeature>()?.Endpoint;
        var attribute = endpoint?.Metadata.GetMetadata<TransactionalAttribute>();
        if (attribute == null) {
            await this._next(httpContext);
            return;
        }

        var transactionService = httpContext.RequestServices.GetRequiredService<ITransactionService>();

        await transactionService.BeginTransactionAsync(attribute.IsolationLevel, httpContext.RequestAborted);

        await this._next(httpContext);

        await transactionService.TryCommitAsync(httpContext.RequestAborted);
    }
}