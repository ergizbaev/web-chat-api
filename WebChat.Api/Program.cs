using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Models;
using Serilog;
using Serilog.Debugging;
using WebChat.Api.Builders;
using WebChat.Api.Extensions;
using WebChat.Api.Filters;
using WebChat.Api.Middlewares;
using WebChat.Application;
using WebChat.Domain.Entities.EntityFramework;
using WebChat.Domain.Shared.Configs;
using WebChat.Infrastructure;
using WebChat.Infrastructure.SignalR.Hubs;

namespace WebChat.Api;

public static class Program {
    public static async Task Main(string[] args) {
        SelfLog.Enable(Console.Error);
        Log.Logger = new LoggerConfiguration().WriteTo.Console().CreateBootstrapLogger();

        var builder = WebApplication.CreateBuilder(args);

        var isDevelopment = builder.Environment.IsDevelopment() || builder.Environment.IsStaging();

        builder.Services.AddControllers();

        builder.Services.AddMvc(options => {
            options.Filters.Add<ExceptionFilter>();
            options.Filters.Add<ValidateModelStateFilter>();
        });

        builder.Services.Configure<ApiBehaviorOptions>(options => { options.SuppressModelStateInvalidFilter = true; });

        builder.Services.AddHttpContextAccessor();
        builder.Services.AddAuthentication();

        builder.Services.AddEndpointsApiExplorer();

        if (isDevelopment) {
            builder.Services.AddSwaggerGen(options => {
                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme {
                    Name = "Authorization",
                    Description = "Enter the Bearer Authorization string as following: `Bearer Generated-JWT-Token`",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                options.AddSecurityRequirement(new OpenApiSecurityRequirement {
                    {
                        new OpenApiSecurityScheme {
                            Name = "Bearer",
                            In = ParameterLocation.Header,
                            Reference = new OpenApiReference {
                                Id = "Bearer",
                                Type = ReferenceType.SecurityScheme
                            }
                        },
                        new List<string>()
                    }
                });
            });   
        }

        builder.Services.AddScoped<IPasswordHasher<User>, PasswordHasher<User>>();

        #region Configs

        builder.Services.MapConfiguration<JwtConfig>(builder.Configuration, "Jwt");
        builder.Services.MapConfiguration<RabbitMqConfig>(builder.Configuration, "RabbitMQ");

        var webHostEnvironment = builder.Services.BuildServiceProvider().GetRequiredService<IWebHostEnvironment>();
        var imagesPath = Path.Combine(webHostEnvironment.ContentRootPath, "wwwroot", "files");
        if (!Directory.Exists(imagesPath))
            Directory.CreateDirectory(imagesPath);

        FileStorageConfig.Path = imagesPath;

        #endregion

        #region Builders

        builder.Services.AddJwtAuthorization();
        builder.Services.AddRabbitMqAndMassTransit();

        #endregion

        #region SignalR

        builder.Services.AddSignalR();

        #endregion

        builder.Services.AddInfrastructure(builder.Configuration);
        builder.Services.AddApplication();

        builder.Host.UseSerilog(ConfigureLogger);
        Log.Information("Web-chat API is successfully started!");

        var app = builder.Build();

        if (isDevelopment) {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        #region Hubs

        app.MapHub<ChatHub>("/hubs/chat");

        #endregion

        app.UseHttpsRedirection();

        app.UseAuthentication();
        app.UseAuthorization();

        app.UseStaticFiles();

        app.UseTransactionMiddleware();
        app.UseSerilogRequestLogger();
        app.UseSerilogRequestLogging();

        app.MapControllers();

        await app.RunAsync();
    }

    private static void ConfigureLogger(HostBuilderContext context, LoggerConfiguration config) {
        config.ReadFrom.Configuration(context.Configuration)
            .Enrich.FromLogContext();
    }
}