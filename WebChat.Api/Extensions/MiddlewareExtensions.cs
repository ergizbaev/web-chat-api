﻿using WebChat.Api.Middlewares;

namespace WebChat.Api.Extensions;

public static class MiddlewareExtensions {
    public static void UseTransactionMiddleware(this IApplicationBuilder app) {
        app.UseMiddleware<TransactionalMiddleware>();
    }

    public static void UseSerilogRequestLogger(this IApplicationBuilder app) {
        app.UseMiddleware<SerilogRequestLogger>();
    }
}