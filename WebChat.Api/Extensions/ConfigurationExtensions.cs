﻿namespace WebChat.Api.Extensions;

public static class ConfigurationExtensions {
    public static void MapConfiguration<T>(this IServiceCollection services, IConfiguration config, string configName) where T : class {
        var configData = config.GetRequiredSection(configName).Get<T>();
        if (configData == null)
            throw new ArgumentException(configName);
        services.AddSingleton(configData);
    }
}