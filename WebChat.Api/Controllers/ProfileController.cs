﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebChat.Api.Controllers.Base;
using WebChat.Application.Handlers.Profile.Commands.ChangeStatus;

namespace WebChat.Api.Controllers;

[Route("api/[controller]")]
[Authorize]
public class ProfileController : BaseApiController {
    public ProfileController(IMediator mediator) : base(mediator) {
    }

    [HttpPatch("status")]
    public async Task<bool> ChangeStatus([FromQuery] ChangeStatusCommandRequest request, CancellationToken cancellationToken) {
        return await this.Mediator.Send(request, cancellationToken);
    }
}