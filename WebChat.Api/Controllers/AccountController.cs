﻿using System.ComponentModel.DataAnnotations;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebChat.Api.Attributes;
using WebChat.Api.Controllers.Base;
using WebChat.Application.DataContracts.Account;
using WebChat.Application.DataContracts.Account.Responses;
using WebChat.Application.DataContracts.Responses;
using WebChat.Application.Handlers.Account.Commands.LoginUser;
using WebChat.Application.Handlers.Account.Commands.RegisterUser;

namespace WebChat.Api.Controllers; 

[Route("api/[controller]/[action]")]
[AllowAnonymous]
public class AccountController : BaseApiController {
    public AccountController(IMediator mediator) : base(mediator) {
    }

    [HttpPost]
    [Transactional]
    public async Task<ApiResponse<AuthResponse>> Register([FromBody][Required] RegisterUserModel request, CancellationToken cancellationToken) {
        var result = await this.Mediator.Send(new RegisterUserCommandRequest(request), cancellationToken);
        return ApiResponse.Success(result);
    }

    [HttpPost]
    [Transactional]
    public async Task<ApiResponse<AuthResponse>> Login([FromBody][Required] LoginUserCommandRequest request, CancellationToken cancellationToken) {
        var result = await this.Mediator.Send(request, cancellationToken);
        return ApiResponse.Success(result);
    }
}