﻿using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebChat.Api.Controllers.Base;

[ApiController]
public abstract class BaseApiController : ControllerBase {
    protected readonly IMediator Mediator;

    protected BaseApiController(IMediator mediator) {
        this.Mediator = mediator;
    }
}