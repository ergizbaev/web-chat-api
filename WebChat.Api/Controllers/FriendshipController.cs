﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebChat.Api.Attributes;
using WebChat.Api.Controllers.Base;
using WebChat.Application.DataContracts.Friendships;
using WebChat.Application.Handlers.Friendships.Commands.AcceptFriendship;
using WebChat.Application.Handlers.Friendships.Commands.DeleteFriend;
using WebChat.Application.Handlers.Friendships.Commands.SendFriendshipRequest;
using WebChat.Application.Handlers.Friendships.Queries.GetFriends;
using WebChat.Application.Handlers.Friendships.Queries.GetFriendshipRequests;

namespace WebChat.Api.Controllers;

[Route("api/[controller]")]
[Authorize]
public class FriendshipController : BaseApiController {

    public FriendshipController(IMediator mediator) : base(mediator) {
    }

    [HttpGet("requests")]
    public async Task<FriendshipRequestUserDto[]> GetRequests(CancellationToken cancellationToken) {
        return await this.Mediator.Send(new GetFriendshipRequestsQuery(), cancellationToken);
    }

    [HttpGet("friends")]
    public async Task<FriendDto[]> GetFriends(CancellationToken cancellationToken) {
        return await this.Mediator.Send(new GetFriendsQuery(), cancellationToken);
    }

    [HttpPost("request")]
    [Transactional]
    public async Task<long> SendRequest([FromBody] SendFriendshipRequestCommandRequest request, CancellationToken cancellationToken) {
        return await this.Mediator.Send(request, cancellationToken);
    }

    [HttpPut("[action]")]
    [Transactional]
    public async Task<bool> Accept([FromBody] AcceptFriendshipCommandRequest request, CancellationToken cancellationToken) {
        return await this.Mediator.Send(request, cancellationToken);
    }

    [HttpDelete("[action]")]
    [Transactional]
    public async Task<bool> Delete([FromQuery] DeleteFriendCommandRequest request, CancellationToken cancellationToken) {
        return await this.Mediator.Send(request, cancellationToken);
    }
}