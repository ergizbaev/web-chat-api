﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebChat.Api.Attributes;
using WebChat.Api.Controllers.Base;
using WebChat.Application.DataContracts.Chats;
using WebChat.Application.DataContracts.Messages;
using WebChat.Application.Handlers.Chats.Commands.CreateGroupChat;
using WebChat.Application.Handlers.Chats.Commands.DeleteMessage;
using WebChat.Application.Handlers.Chats.Commands.EditMessage;
using WebChat.Application.Handlers.Chats.Commands.InviteToGroupChat;
using WebChat.Application.Handlers.Chats.Commands.SendMessage;
using WebChat.Application.Handlers.Chats.Queries.GetAllChats;
using WebChat.Application.Handlers.Chats.Queries.GetMessagesByChat;

namespace WebChat.Api.Controllers;

[Route("api/[controller]")]
[Authorize]
public class ChatController : BaseApiController {
    public ChatController(IMediator mediator) : base(mediator) { }

    [HttpGet]
    public async Task<IQueryable<ChatDto>> GetChats(CancellationToken cancellationToken) {
        return await this.Mediator.Send(new GetAllChatsQuery(), cancellationToken);
    }

    [HttpGet("{id:long}/messages")]
    public async Task<MessageDto[]> GetMessagesByChat(long id, CancellationToken cancellationToken) {
        return await this.Mediator.Send(new GetMessagesByChatQuery(id), cancellationToken);
    }

    [HttpPost("send")]
    [Transactional]
    public async Task<long> SendMessage([FromForm] SendMessageCommandRequest request, CancellationToken cancellationToken) {
        return await this.Mediator.Send(request, cancellationToken);
    }

    [HttpPost("create/group")]
    [Transactional]
    public async Task<long> CreateGroupChat([FromBody] CreateGroupChatCommandRequest request, CancellationToken cancellationToken) {
        return await this.Mediator.Send(request, cancellationToken);
    }

    [HttpPost("invite")]
    [Transactional]
    public async Task<bool> InviteToGroupChat([FromBody] InviteToGroupChatCommandRequest request, CancellationToken cancellationToken) {
        return await this.Mediator.Send(request, cancellationToken);
    }

    [HttpPatch("message/edit")]
    [Transactional]
    public async Task<bool> EditMessage([FromBody] EditMessageCommandRequest request, CancellationToken cancellationToken) {
        return await this.Mediator.Send(request, cancellationToken);
    }

    [HttpDelete("message/delete")]
    [Transactional]
    public async Task<bool> DeleteMessage([FromQuery] DeleteMessageCommandRequest request, CancellationToken cancellationToken) {
        return await this.Mediator.Send(request, cancellationToken);
    }
}