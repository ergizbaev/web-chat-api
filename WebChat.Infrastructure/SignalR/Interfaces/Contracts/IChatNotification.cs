﻿namespace WebChat.Infrastructure.SignalR.Interfaces.Contracts; 

public interface IChatNotification {
    Task OnUserStatusChange(object data, CancellationToken cancellationToken);
}