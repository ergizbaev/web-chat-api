﻿using System.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebChat.Domain.Constants.Enum;
using WebChat.Infrastructure.Databases.EntityFramework;
using WebChat.Infrastructure.Databases.EntityFramework.Repositories.Interfaces;
using WebChat.Infrastructure.Identity.Services.Interfaces;
using WebChat.Infrastructure.Services.Interfaces;
using WebChat.Infrastructure.SignalR.Interfaces.Contracts;

namespace WebChat.Infrastructure.SignalR.Hubs;

[Authorize]
public class ChatHub : Hub<IChatNotification> {
    private readonly AppDbContext _dbContext;
    private readonly ILogger<ChatHub> _logger;
    private readonly IChatRepository _chatRepository;
    private readonly ICurrentUserService _currentUserService;
    private readonly IFriendshipRepository _friendshipRepository;

    public ChatHub(
        AppDbContext dbContext,
        ILogger<ChatHub> logger,
        IChatRepository chatRepository,
        ICurrentUserService currentUserService,
        IFriendshipRepository friendshipRepository
    ) {
        this._dbContext = dbContext;
        this._logger = logger;
        this._chatRepository = chatRepository;
        this._currentUserService = currentUserService;
        this._friendshipRepository = friendshipRepository;
    }

    public override async Task OnConnectedAsync() {
        var transaction = await this._dbContext.Database.BeginTransactionAsync(IsolationLevel.ReadCommitted, this.Context.ConnectionAborted);

        try {
            await this.OnUserConnected();
            await transaction.CommitAsync(this.Context.ConnectionAborted);
        }
        catch (Exception exception) {
            this._logger.LogError(exception, exception.Message);
            await transaction.RollbackAsync();
            this.Context.Abort();
        }
    }

    public override async Task OnDisconnectedAsync(Exception? exception) {
        if (exception != null) {
            this._logger.LogError(exception, $"Unhandled SignalR disconnect error: {exception.Message}");
        }
        
        await this.Logout();
    }

    private async Task OnUserConnected() {
        await this._dbContext.Users
            .Where(x => x.Id == this._currentUserService.UserId)
            .ExecuteUpdateAsync(x =>
                    x.SetProperty(p => p.Status, UserStatusEnum.Online)
                     .SetProperty(p => p.ConnectionId, this.Context.ConnectionId)
                     .SetProperty(p => p.LastOnlineDate, default(DateTimeOffset?)),
                this.Context.ConnectionAborted);

        await this.AddToChatGroups();
        await this.NotificateFriends(UserStatusEnum.Online, this.Context.ConnectionAborted);
    }

    private async Task AddToChatGroups() {
        var chatIds = await this._chatRepository
                           .GetChatsByType(ChatType.Group)
                           .Select(x => x.Id)
                           .ToArrayAsync(this.Context.ConnectionAborted);

        await Parallel.ForEachAsync(chatIds, async (chatId, _) => {
            await this.Groups.AddToGroupAsync(this.Context.ConnectionId, $"GroupChat-{chatId}", this.Context.ConnectionAborted);
        });
    }

    private async Task Logout() {
        var userId = this._currentUserService.UserId;

        await this._dbContext.Users
            .Where(x => x.Id == userId)
            .ExecuteUpdateAsync(x =>
                x.SetProperty(p => p.LastOnlineDate, DateTimeOffset.UtcNow)
                 .SetProperty(p => p.Status, UserStatusEnum.Offline));

        await this.NotificateFriends(UserStatusEnum.Offline);
    }

    private async Task NotificateFriends(UserStatusEnum userStatus, CancellationToken cancellationToken = default) {
        var onlineFriendsConnectionIds = await this._friendshipRepository
            .GetFriends()
            .Where(x => x.Status != UserStatusEnum.Offline)
            .Select(x => x.ConnectionId)
            .ToArrayAsync(cancellationToken);

        if (!onlineFriendsConnectionIds.Any())
            return;

        await this.Clients.Clients(onlineFriendsConnectionIds!).OnUserStatusChange(new {
            this._currentUserService.UserId,
            Status = userStatus
        }, cancellationToken);
    }
}