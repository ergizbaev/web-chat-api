﻿using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Entities.EntityFramework;

namespace WebChat.Infrastructure.Services.Interfaces;

public interface IChatRepository {
    IQueryable<Chat> GetChatsByType(ChatType chatType);
}