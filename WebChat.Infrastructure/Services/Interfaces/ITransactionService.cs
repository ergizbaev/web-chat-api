﻿using System.Data;

namespace WebChat.Infrastructure.Services.Interfaces;

public interface ITransactionService {
    Task BeginTransactionAsync(IsolationLevel isolationLevel, CancellationToken cancellationToken = default);
    Task TryCommitAsync(CancellationToken cancellationToken = default);
    Task TryRollbackAsync();
}