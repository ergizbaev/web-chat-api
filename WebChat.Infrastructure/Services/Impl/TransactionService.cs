﻿using System.Data;
using Microsoft.EntityFrameworkCore;
using WebChat.Domain.Shared.Helpers;
using WebChat.Infrastructure.Databases.EntityFramework;
using WebChat.Infrastructure.Services.Interfaces;

namespace WebChat.Infrastructure.Services.Impl;

public class TransactionService : ITransactionService {
    private readonly AppDbContext _dbContext;

    public TransactionService(AppDbContext dbContext) {
        this._dbContext = dbContext;
    }

    private bool HasActiveTransaction {
        get { return this._dbContext.Database.CurrentTransaction != null; }
    }

    public async Task BeginTransactionAsync(IsolationLevel isolationLevel, CancellationToken cancellationToken = default) {
        if (this.HasActiveTransaction)
            ThrowHelper.ThrowApiException("Необходимо завершить остальные транзакции");

        await this._dbContext.Database.BeginTransactionAsync(isolationLevel, cancellationToken);
    }

    public async Task TryCommitAsync(CancellationToken cancellationToken = default) {
        if (this.HasActiveTransaction)
            await this._dbContext.Database.CurrentTransaction.CommitAsync(cancellationToken);
    }

    public async Task TryRollbackAsync() {
        if (this.HasActiveTransaction)
            await this._dbContext.Database.CurrentTransaction.RollbackAsync();
    }
}