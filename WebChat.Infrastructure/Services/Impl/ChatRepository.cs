﻿using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Entities.EntityFramework;
using WebChat.Infrastructure.Databases.EntityFramework;
using WebChat.Infrastructure.Identity.Services.Interfaces;
using WebChat.Infrastructure.Services.Interfaces;

namespace WebChat.Infrastructure.Services.Impl; 

public class ChatRepository : IChatRepository {
    private readonly AppDbContext _dbContext;
    private readonly ICurrentUserService _currentUserService;

    public ChatRepository(AppDbContext dbContext, ICurrentUserService currentUserService) {
        this._dbContext = dbContext;
        this._currentUserService = currentUserService;
    }

    public IQueryable<Chat> GetChatsByType(ChatType chatType) {
        return this._dbContext.Users
                   .Where(x => x.Id == this._currentUserService.UserId)
                   .SelectMany(x => x.Chats)
                   .Where(x => x.ChatType == chatType);
    }
}