﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WebChat.Infrastructure.Databases.EntityFramework;
using WebChat.Infrastructure.Databases.EntityFramework.Repositories.Impl;
using WebChat.Infrastructure.Databases.EntityFramework.Repositories.Interfaces;
using WebChat.Infrastructure.Identity.Services.Impl;
using WebChat.Infrastructure.Identity.Services.Interfaces;
using WebChat.Infrastructure.Services.Impl;
using WebChat.Infrastructure.Services.Interfaces;

namespace WebChat.Infrastructure;

public static class DependencyInjection {
    public static void AddInfrastructure(this IServiceCollection services, IConfiguration config) {
        var connectionString = config.GetConnectionString("DbConnection");

        services.AddDbContextPool<AppDbContext>(options => {
            options.UseNpgsql(connectionString, npgsqlOptions => {
                npgsqlOptions.MaxBatchSize(100);
                npgsqlOptions.UseNetTopologySuite();
            }).UseLazyLoadingProxies();
        });

        #region IdentityServices

        services.AddScoped<IJwtGeneratorService, JwtGeneratorService>();
        services.AddScoped<ICurrentUserService, CurrentUserService>();
        services.AddScoped<ICurrentUserExtendedService, CurrentUserServiceExtended>();

        #endregion

        #region DatabaseServices

        services.AddScoped<ITransactionService, TransactionService>();

        #endregion

        #region Repositories

        services.AddScoped<IChatRepository, ChatRepository>();
        services.AddScoped<IFriendshipRepository, FriendshipRepository>();

        #endregion

        #if !DEBUG
            MigrateDatabase(services);
        #endif
    }
    
    private static void MigrateDatabase(in IServiceCollection services) {
        var dbContext = services.BuildServiceProvider().GetRequiredService<AppDbContext>();
        dbContext.Database.Migrate();
    }
}