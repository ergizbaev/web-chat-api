﻿using System.Linq.Expressions;
using WebChat.Domain.Entities.EntityFramework;

namespace WebChat.Infrastructure.Databases.EntityFramework.Repositories.Interfaces;

public interface IFriendshipRepository {
    IQueryable<Friendship> GetFriendship(long userId);
    IQueryable<Friendship> GetAllRequests();
    IQueryable<User> GetFriends(Expression<Func<User, bool>>? predicate = null);
}