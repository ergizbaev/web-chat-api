﻿using System.Linq.Expressions;
using WebChat.Domain.Entities.EntityFramework;
using WebChat.Infrastructure.Databases.EntityFramework.Repositories.Interfaces;
using WebChat.Infrastructure.Identity.Services.Interfaces;

namespace WebChat.Infrastructure.Databases.EntityFramework.Repositories.Impl;

public class FriendshipRepository : IFriendshipRepository {
    private readonly ICurrentUserService _currentUserService;
    private readonly AppDbContext _dbContext;

    public FriendshipRepository(AppDbContext dbContext, ICurrentUserService currentUserService) {
        this._dbContext = dbContext;
        this._currentUserService = currentUserService;
    }

    public IQueryable<Friendship> GetFriendship(long userId) {
        var currentUserId = this._currentUserService.UserId;
        return this._dbContext.Users
            .Where(x => x.Id == currentUserId)
            .SelectMany(x => x.Friendships)
            .Where(x => x.Members.Any(m => m.Id == userId));
    }

    public IQueryable<Friendship> GetAllRequests() {
        var userId = this._currentUserService.UserId;
        return this._dbContext.Users
            .Where(x => x.Id == userId)
            .SelectMany(x => x.Friendships);
    }

    public IQueryable<User> GetFriends(Expression<Func<User, bool>>? predicate) {
        var userId = this._currentUserService.UserId;
        return this._dbContext.Friendships
                   .Where(x => x.Members.Any(m => m.Id == userId) && x.Accepted)
                   .SelectMany(x => x.Members)
                   .Where(x => x.Id != userId)
                   .Where(predicate!);
    }
}