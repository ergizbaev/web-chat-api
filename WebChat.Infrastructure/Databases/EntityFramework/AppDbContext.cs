﻿using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using WebChat.Domain.Entities.EntityFramework;
using WebChat.Domain.Entities.EntityFramework.Base;

namespace WebChat.Infrastructure.Databases.EntityFramework;

public class AppDbContext : DbContext {
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

    public DbSet<User> Users { get; set; }
    public DbSet<Chat> Chats { get; set; }
    public DbSet<Message> Messages { get; set; }
    public DbSet<MessageFile> MessageFiles { get; set; }

    public DbSet<Friendship> Friendships { get; set; }

    public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new()) {
        foreach (var entry in this.ChangeTracker.Entries<BaseEntity>()) {
            switch (entry.State) {
                case EntityState.Added: {
                    entry.Entity.CreatedAt = DateTimeOffset.UtcNow;
                    break;
                }
                case EntityState.Modified: {
                    entry.Entity.UpdateAt = DateTimeOffset.UtcNow;
                    break;
                }
                case EntityState.Deleted: {
                    entry.State = EntityState.Modified;
                    entry.Entity.IsDeleted = true;
                    entry.Entity.DeletedAt = DateTimeOffset.UtcNow;
                    break;
                }
            }
        }

        foreach (var entry in this.ChangeTracker.Entries<BaseEntity>()) {
            switch (entry.State) {
                case EntityState.Deleted: {
                    entry.State = EntityState.Modified;
                    entry.Entity.IsDeleted = true;
                    break;
                }
            }
        }
        return base.SaveChangesAsync(cancellationToken);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder) {
        base.OnModelCreating(modelBuilder);
        
        EnableSoftDeletionFilter(modelBuilder);

        #region IndexFilters

        modelBuilder.Entity<User>().HasIndex(x => new { x.Email }).HasFilter(@"""IsDeleted"" = false").IsUnique();
        modelBuilder.Entity<Message>().HasIndex(x => new { x.Text });

        modelBuilder.Entity<Message>()
                    .HasMany(x => x.Files)
                    .WithOne(x => x.Message)
                    .OnDelete(DeleteBehavior.Cascade);

        #endregion

        #region HiLO
        
        modelBuilder.Entity<Message>().Property(x => x.Id).UseHiLo();
        modelBuilder.Entity<MessageFile>().Property(x => x.Id).UseHiLo();

        #endregion

        modelBuilder.HasPostgresExtension("postgis");
    }

    private static void EnableSoftDeletionFilter(in ModelBuilder modelBuilder) {
        var softDeleteEntities = typeof(BaseEntity).Assembly.GetTypes()
                                    .Where(type =>
                                        typeof(BaseEntity).IsAssignableFrom(type)
                                        && type is { IsClass: true, IsAbstract: false });

        foreach (var softDeleteEntity in softDeleteEntities)
        {
            modelBuilder.Entity(softDeleteEntity).HasQueryFilter(GenerateSoftDeletionQueryFilterLambdaExpression(softDeleteEntity));
        }
    }
    
    private static LambdaExpression GenerateSoftDeletionQueryFilterLambdaExpression(Type type)
    {
        var parameter = Expression.Parameter(type, "e");
        var falseConstant = Expression.Constant(false);
        var propertyAccess = Expression.PropertyOrField(parameter, nameof(BaseEntity.IsDeleted));
        var equalExpression = Expression.Equal(propertyAccess, falseConstant);

        var lambda = Expression.Lambda(equalExpression, parameter);

        return lambda;
    }
}