﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using WebChat.Domain.Shared.Configs;
using WebChat.Infrastructure.Identity.Services.Interfaces;

namespace WebChat.Infrastructure.Identity.Services.Impl;

public class JwtGeneratorService : IJwtGeneratorService {
    private readonly JwtConfig _jwtConfig;

    public JwtGeneratorService(JwtConfig jwtConfig) {
        this._jwtConfig = jwtConfig;
    }

    public Task<string> GenerateJwt(in IEnumerable<Claim> claims) {
        var key = Encoding.ASCII.GetBytes(this._jwtConfig.SecretKey);
        var signingCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha512Signature);

        var token = new JwtSecurityToken(
            this._jwtConfig.Issuer,
            this._jwtConfig.Audience,
            claims,
            expires: DateTime.Now.AddDays(7),
            signingCredentials: signingCredentials);

        return Task.FromResult(new JwtSecurityTokenHandler().WriteToken(token));
    }
}