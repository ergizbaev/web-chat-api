﻿using WebChat.Domain.Entities.EntityFramework;
using WebChat.Infrastructure.Databases.EntityFramework;
using WebChat.Infrastructure.Identity.Services.Interfaces;

namespace WebChat.Infrastructure.Identity.Services.Impl;

public class CurrentUserServiceExtended : ICurrentUserExtendedService {
    private readonly ICurrentUserService _currentUserService;
    private readonly AppDbContext _dbContext;

    public CurrentUserServiceExtended(ICurrentUserService currentUserService, AppDbContext dbContext) {
        this._currentUserService = currentUserService;
        this._dbContext = dbContext;
    }

    public ValueTask<User> GetUserEntity(CancellationToken cancellationToken = default) {
        return this._dbContext.Users.FindAsync(new object[] { this._currentUserService.UserId }, cancellationToken)!;
    }
}