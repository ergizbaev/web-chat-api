﻿using Microsoft.AspNetCore.Http;
using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Shared.Helpers;
using WebChat.Infrastructure.Identity.Services.Interfaces;

namespace WebChat.Infrastructure.Identity.Services.Impl;

public class CurrentUserService : ICurrentUserService {

    public CurrentUserService(IHttpContextAccessor httpContextAccessor) {
        if (long.TryParse(httpContextAccessor.HttpContext?.User.Identity?.Name!, out var userId))
            this.UserId = userId;
        else
            ThrowHelper.ThrowApiException("Необходимо войти в систему", ApiErrorCode.UnauthorizedError);
    }

    public long UserId { get; }
}