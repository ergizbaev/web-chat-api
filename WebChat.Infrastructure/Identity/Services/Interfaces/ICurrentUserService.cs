﻿namespace WebChat.Infrastructure.Identity.Services.Interfaces;

public interface ICurrentUserService {
    long UserId { get; }
}