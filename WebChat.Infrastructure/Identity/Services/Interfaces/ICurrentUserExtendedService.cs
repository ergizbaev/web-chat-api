﻿using WebChat.Domain.Entities.EntityFramework;

namespace WebChat.Infrastructure.Identity.Services.Interfaces;

public interface ICurrentUserExtendedService {
    ValueTask<User> GetUserEntity(CancellationToken cancellationToken = default);
}