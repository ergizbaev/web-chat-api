﻿using System.Security.Claims;

namespace WebChat.Infrastructure.Identity.Services.Interfaces;

public interface IJwtGeneratorService {
    Task<string> GenerateJwt(in IEnumerable<Claim> claims);
}