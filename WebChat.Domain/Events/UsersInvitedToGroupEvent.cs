﻿namespace WebChat.Domain.Events;

public class UsersInvitedToGroupEvent {
    public long InviterId { get; set; }
    public long ChatId { get; set; }
    public long[] UsersIds { get; set; }
}