﻿using WebChat.Domain.Constants.Enum;

namespace WebChat.Domain.Shared.Exceptions;

public class ApiException : Exception {
    public ApiException(string errorText, ApiErrorCode errorCode = ApiErrorCode.UnknownError) : base(errorText) {
        this.ErrorCode = errorCode;
    }

    public ApiErrorCode ErrorCode { get; }
}