﻿namespace WebChat.Domain.Shared.Configs;

public class RabbitMqConfig {
    public string Url { get; set; }
    public string VirtualHost { get; set; }
    public string UserName { get; set; }
    public string Password { get; set; }
}