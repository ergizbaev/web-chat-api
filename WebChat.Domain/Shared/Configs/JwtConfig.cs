﻿namespace WebChat.Domain.Shared.Configs;

public class JwtConfig {
    public string Audience { get; set; }
    public string Issuer { get; set; }
    public string SecretKey { get; set; }
}