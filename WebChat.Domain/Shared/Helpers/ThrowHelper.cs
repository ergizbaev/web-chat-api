﻿using System.Diagnostics.CodeAnalysis;
using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Shared.Exceptions;

namespace WebChat.Domain.Shared.Helpers;

public static class ThrowHelper {
    [DoesNotReturn]
    public static void ThrowApiException(in string errorText, in ApiErrorCode errorCode = ApiErrorCode.UnknownError) {
        throw new ApiException(errorText, errorCode);
    }
}