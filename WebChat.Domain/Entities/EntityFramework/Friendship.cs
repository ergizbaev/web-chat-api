﻿using WebChat.Domain.Entities.EntityFramework.Base;

namespace WebChat.Domain.Entities.EntityFramework;

public class Friendship : BaseEntity {

    public Friendship() {
        this.Members = new List<User>();
    }

    public Friendship(in User requestor) : this() {
        this.RequestorId = requestor.Id;
        this.Members.Add(requestor);
    }

    public bool Accepted { get; set; }

    /// <summary>
    ///     ID пользователя, который отправил запрос на дружбу
    /// </summary>
    public long RequestorId { get; set; }
    public virtual ICollection<User> Members { get; set; }
}