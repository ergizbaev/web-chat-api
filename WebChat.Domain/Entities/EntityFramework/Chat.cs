﻿using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Entities.EntityFramework.Base;

namespace WebChat.Domain.Entities.EntityFramework;

public class Chat : BaseEntity {
    public Chat() {
        this.Members = new List<User>();
        this.Messages = new List<Message>();
    }

    public Chat(in User[] members, in ChatType chatType) : this() {
        this.Members = members;
        this.ChatType = chatType;
    }

    public Chat(in string title, in ICollection<User> members) : this() {
        this.Title = title;
        this.Members = members;
        this.ChatType = ChatType.Group;
    }

    public string? Title { get; set; }
    public ChatType ChatType { get; set; }
    public virtual ICollection<User> Members { get; set; }
    public virtual ICollection<Message> Messages { get; set; }
}