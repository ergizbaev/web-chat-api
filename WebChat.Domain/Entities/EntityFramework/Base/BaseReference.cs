﻿namespace WebChat.Domain.Entities.EntityFramework.Base; 

public abstract class BaseReference<TEnum> where TEnum: Enum {
    public long Id { get; set; }
    public bool IsDeleted { get; set; }
    public TEnum Code { get; set; }
}