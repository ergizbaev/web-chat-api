﻿namespace WebChat.Domain.Entities.EntityFramework.Base;

public abstract class BaseEntity {
    public long Id { get; set; }
    public bool IsDeleted { get; set; }
    public DateTimeOffset CreatedAt { get; set; }
    public DateTimeOffset? UpdateAt { get; set; }
    public DateTimeOffset? DeletedAt { get; set; }
}