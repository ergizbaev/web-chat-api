﻿using WebChat.Domain.Entities.EntityFramework.Base;

namespace WebChat.Domain.Entities.EntityFramework;

public class Message : BaseEntity {
    public Message() {
        this.Files = new List<MessageFile>();
    }

    public Message(in User sender, in string text) : this() {
        this.Sender = sender;
        this.Text = text;
    }

    public string Text { get; set; }
    public bool IsEdited { get; set; }
    public virtual User Sender { get; protected set; }
    public virtual ICollection<MessageFile> Files { get; set; }
}