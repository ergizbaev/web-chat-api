using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Entities.EntityFramework.Base;

namespace WebChat.Domain.Entities.EntityFramework; 

public class Permission : BaseEntity {
    public PermissionEnum Code { get; set; }
    public virtual ISet<User> Users { get; set; }

    public Permission() {
        this.Users = new HashSet<User>();
    }
}