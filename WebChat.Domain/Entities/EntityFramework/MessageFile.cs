﻿using WebChat.Domain.Entities.EntityFramework.Base;

namespace WebChat.Domain.Entities.EntityFramework;

public class MessageFile : BaseEntity {

    public MessageFile() {
    }

    public MessageFile(in string name, in string filePath) {
        this.Name = name;
        this.Path = filePath;
    }
    
    public virtual Message Message { get; protected set; }

    public string Name { get; protected set; }
    public string Path { get; protected set; }
}