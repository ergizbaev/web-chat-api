﻿using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Entities.EntityFramework.Base;

namespace WebChat.Domain.Entities.EntityFramework;

public class User : BaseEntity {
    public User() {
        this.Chats = new HashSet<Chat>();
        this.Friendships = new HashSet<Friendship>();
    }

    public string Email { get; set; }
    public string Password { get; set; }
    public string FirstName { get; set; }
    public string? LastName { get; set; }
    public string? Photo { get; set; }
    public string? ConnectionId { get; set; }
    public UserStatusEnum Status { get; set; }
    public DateTimeOffset? LastOnlineDate { get; set; }
    public virtual ISet<Permission> Permissions { get; set; }
    public virtual ISet<Chat> Chats { get; set; }
    public virtual ISet<Friendship> Friendships { get; set; }
}