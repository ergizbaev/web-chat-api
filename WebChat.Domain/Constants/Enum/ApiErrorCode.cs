﻿namespace WebChat.Domain.Constants.Enum;

public enum ApiErrorCode : ushort {
    ValidationError = 400,
    UnauthorizedError = 401,
    UnknownError = 500
}