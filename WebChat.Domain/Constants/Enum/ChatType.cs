﻿namespace WebChat.Domain.Constants.Enum;

public enum ChatType : byte {
    Private = 0,
    Group = 1
}