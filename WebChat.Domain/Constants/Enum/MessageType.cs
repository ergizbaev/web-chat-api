﻿namespace WebChat.Domain.Constants.Enum;

public enum MessageType : byte {
    Default = 0,
    Event = 1,
    PersonalEvent = 2
}