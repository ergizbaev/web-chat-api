namespace WebChat.Domain.Constants.Enum; 

public enum PermissionEnum : ushort {
    Admin = 0,
    Student = 1
}