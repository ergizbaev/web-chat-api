﻿namespace WebChat.Domain.Constants.Enum;

public enum UserStatusEnum : byte {
    Offline = 0,
    Online = 1,
    Busy = 2,
    Invisible = 3
}