﻿namespace WebChat.Domain.Constants.Enum.Reference; 

public enum SpecializationEnum : byte {
    Surveyor = 10,
    Electric = 20,
    Programmer = 30
}