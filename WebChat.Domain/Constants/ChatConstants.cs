﻿namespace WebChat.Domain.Constants;

public static class ChatConstants {
    public const byte MaxMemberCount = 50;
}