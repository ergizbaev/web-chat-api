﻿namespace WebChat.Domain.Constants;

public static class QueueNames {
    public const string InviteUsersToGroup = "invite-users-to-group";
}