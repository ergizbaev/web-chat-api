﻿using System.Reflection;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using WebChat.Application.Common.Behaviours;
using WebChat.Application.Common.Interfaces;
using WebChat.Application.DataContracts.Account;
using WebChat.Application.Handlers.Account.Commands.RegisterUser;

namespace WebChat.Application;

public static class DependencyInjection {
    public static void AddApplication(this IServiceCollection services) {
        services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidatorBehaviour<,>));
        services.AddAutoMapper(options => { options.AllowNullCollections = true; },
            typeof(RegisterUserModel), typeof(RegisterUserCommandRequest));

        services.Scan(
            x => {
                var entryAssembly = Assembly.GetEntryAssembly();
                var referencedAssemblies = entryAssembly?.GetReferencedAssemblies().Select(Assembly.Load);
                var assemblies = new List<Assembly> { entryAssembly }.Concat(referencedAssemblies!);

                x.FromAssemblies(assemblies)
                    .AddClasses(classes => classes.AssignableTo(typeof(IRequestValidator<>)))
                    .AsImplementedInterfaces()
                    .WithScopedLifetime();
            });

        services.AddMediatR(typeof(RegisterUserCommandRequest));
    }
}