﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebChat.Application.DataContracts.Friendships;
using WebChat.Infrastructure.Databases.EntityFramework.Repositories.Interfaces;
using WebChat.Infrastructure.Identity.Services.Interfaces;

namespace WebChat.Application.Handlers.Friendships.Queries.GetFriendshipRequests;

public class GetFriendshipRequestsQueryHandler : IRequestHandler<GetFriendshipRequestsQuery, FriendshipRequestUserDto[]> {
    private readonly ICurrentUserService _currentUserService;
    private readonly IFriendshipRepository _friendshipRepository;
    private readonly IMapper _mapper;

    public GetFriendshipRequestsQueryHandler(IMapper mapper, ICurrentUserService currentUserService, IFriendshipRepository friendshipRepository) {
        this._mapper = mapper;
        this._currentUserService = currentUserService;
        this._friendshipRepository = friendshipRepository;
    }

    public Task<FriendshipRequestUserDto[]> Handle(GetFriendshipRequestsQuery request, CancellationToken cancellationToken) {
        var userId = this._currentUserService.UserId;

        return this._friendshipRepository.GetAllRequests()
            .Where(x => x.RequestorId != userId && !x.Accepted)
            .SelectMany(x => x.Members)
            .Where(x => x.Id != userId)
            .ProjectTo<FriendshipRequestUserDto>(this._mapper.ConfigurationProvider)
            .ToArrayAsync(cancellationToken);
    }
}