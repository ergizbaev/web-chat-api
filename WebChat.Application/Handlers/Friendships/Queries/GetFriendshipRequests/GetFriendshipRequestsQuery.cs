﻿using MediatR;
using WebChat.Application.DataContracts.Friendships;

namespace WebChat.Application.Handlers.Friendships.Queries.GetFriendshipRequests;

public class GetFriendshipRequestsQuery : IRequest<FriendshipRequestUserDto[]> {
}