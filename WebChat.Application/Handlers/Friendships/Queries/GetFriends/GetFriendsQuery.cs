﻿using MediatR;
using WebChat.Application.DataContracts.Friendships;

namespace WebChat.Application.Handlers.Friendships.Queries.GetFriends;

public class GetFriendsQuery : IRequest<FriendDto[]> {
}