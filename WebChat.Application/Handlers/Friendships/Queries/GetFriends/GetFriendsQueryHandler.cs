﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebChat.Application.DataContracts.Friendships;
using WebChat.Infrastructure.Databases.EntityFramework.Repositories.Interfaces;

namespace WebChat.Application.Handlers.Friendships.Queries.GetFriends;

public class GetFriendsQueryHandler : IRequestHandler<GetFriendsQuery, FriendDto[]> {
    private readonly IFriendshipRepository _friendshipRepository;
    private readonly IMapper _mapper;

    public GetFriendsQueryHandler(IMapper mapper, IFriendshipRepository chatRepository) {
        this._mapper = mapper;
        this._friendshipRepository = chatRepository;
    }

    public async Task<FriendDto[]> Handle(GetFriendsQuery request, CancellationToken cancellationToken) {
        return await this._friendshipRepository.GetFriends()
            .ProjectTo<FriendDto>(this._mapper.ConfigurationProvider)
            .ToArrayAsync(cancellationToken);
    }
}