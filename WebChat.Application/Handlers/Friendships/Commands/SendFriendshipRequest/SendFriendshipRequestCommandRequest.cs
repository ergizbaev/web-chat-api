﻿using System.ComponentModel.DataAnnotations;
using MediatR;

namespace WebChat.Application.Handlers.Friendships.Commands.SendFriendshipRequest;

public class SendFriendshipRequestCommandRequest : IRequest<long> {
    [Required]
    public long FriendshipReceiverId { get; set; }
}