﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using WebChat.Application.Common.Impl;
using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Shared.Helpers;
using WebChat.Infrastructure.Databases.EntityFramework;
using WebChat.Infrastructure.Databases.EntityFramework.Repositories.Interfaces;
using WebChat.Infrastructure.Identity.Services.Interfaces;

namespace WebChat.Application.Handlers.Friendships.Commands.SendFriendshipRequest;

public class SendFriendshipRequestCommandValidator : RequestValidator<SendFriendshipRequestCommandRequest> {
    private readonly AppDbContext _dbContext;
    private readonly IFriendshipRepository _friendshipRepository;

    public SendFriendshipRequestCommandValidator(AppDbContext dbContext, IFriendshipRepository chatRepository, ICurrentUserService currentUserService) {
        this._dbContext = dbContext;
        this._friendshipRepository = chatRepository;

        this.RuleFor(x => x.FriendshipReceiverId)
            .Must(x => x != currentUserService.UserId)
            .WithMessage("Нельзя самого себя добавить в друзья");
    }

    public override async Task ValidateAsync(SendFriendshipRequestCommandRequest request, CancellationToken cancellationToken) {
        var userHasExists = await this._dbContext.Users.AnyAsync(x => x.Id == request.FriendshipReceiverId, cancellationToken);
        if (!userHasExists)
            ThrowHelper.ThrowApiException("Пользователь не найден", ApiErrorCode.ValidationError);

        var isAlreadyFriends = await this._friendshipRepository
            .GetAllRequests()
            .SelectMany(x => x.Members)
            .AnyAsync(x => x.Id == request.FriendshipReceiverId, cancellationToken);
        if (isAlreadyFriends)
            ThrowHelper.ThrowApiException("Вы уже являетесь друзьями, либо уже отправляли запрос на дружбу", ApiErrorCode.ValidationError);
    }
}