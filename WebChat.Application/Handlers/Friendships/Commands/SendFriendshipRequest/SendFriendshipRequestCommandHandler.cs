﻿using MediatR;
using WebChat.Domain.Entities.EntityFramework;
using WebChat.Infrastructure.Databases.EntityFramework;
using WebChat.Infrastructure.Identity.Services.Interfaces;

namespace WebChat.Application.Handlers.Friendships.Commands.SendFriendshipRequest;

public class SendFriendshipRequestCommandHandler : IRequestHandler<SendFriendshipRequestCommandRequest, long> {
    private readonly ICurrentUserExtendedService _currentUserService;
    private readonly AppDbContext _dbContext;

    public SendFriendshipRequestCommandHandler(AppDbContext dbContext, ICurrentUserExtendedService currentUserService) {
        this._dbContext = dbContext;
        this._currentUserService = currentUserService;
    }

    public async Task<long> Handle(SendFriendshipRequestCommandRequest request, CancellationToken cancellationToken) {
        var requestor = await this._currentUserService.GetUserEntity(cancellationToken);
        var friendshipReceiver = await this._dbContext.Users.FindAsync(new object[] { request.FriendshipReceiverId }, cancellationToken);

        var friendship = new Friendship(requestor);
        friendship.Members.Add(friendshipReceiver);

        this._dbContext.Friendships.Add(friendship);
        await this._dbContext.SaveChangesAsync(cancellationToken);

        return friendship.Id;
    }
}