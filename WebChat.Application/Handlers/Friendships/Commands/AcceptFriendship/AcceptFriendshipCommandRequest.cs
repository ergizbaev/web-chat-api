﻿using MediatR;

namespace WebChat.Application.Handlers.Friendships.Commands.AcceptFriendship;

public class AcceptFriendshipCommandRequest : IRequest<bool> {
    public long UserId { get; set; }
}