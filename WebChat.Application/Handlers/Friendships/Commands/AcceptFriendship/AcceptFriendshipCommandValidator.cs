﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using WebChat.Application.Common.Impl;
using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Shared.Helpers;
using WebChat.Infrastructure.Databases.EntityFramework.Repositories.Interfaces;
using WebChat.Infrastructure.Identity.Services.Interfaces;

namespace WebChat.Application.Handlers.Friendships.Commands.AcceptFriendship;

public class AcceptFriendshipCommandValidator : RequestValidator<AcceptFriendshipCommandRequest> {
    private readonly ICurrentUserService _currentUserService;
    private readonly IFriendshipRepository _friendshipRepository;

    public AcceptFriendshipCommandValidator(IFriendshipRepository friendshipRepository, ICurrentUserService currentUserService) {
        this._currentUserService = currentUserService;
        this._friendshipRepository = friendshipRepository;

        this.RuleFor(x => x.UserId).Must(x => x != currentUserService.UserId);
        this.RuleFor(x => x.UserId).NotEmpty().WithMessage("Выберите запрос на дружбу");
    }

    public override async Task ValidateAsync(AcceptFriendshipCommandRequest request, CancellationToken cancellationToken) {
        var friendshipRequestExists = await this._friendshipRepository
            .GetAllRequests()
            .Where(x => !x.Accepted)
            .SelectMany(x => x.Members)
            .AnyAsync(x => x.Id == request.UserId, cancellationToken);
        if (!friendshipRequestExists)
            ThrowHelper.ThrowApiException("Не найден запрос на дружбу", ApiErrorCode.ValidationError);

        var friendship = await this._friendshipRepository
            .GetAllRequests()
            .Where(x => x.Members.Any(m => m.Id == request.UserId))
            .Select(x => new { x.RequestorId })
            .SingleAsync(cancellationToken);

        if (friendship.RequestorId == this._currentUserService.UserId)
            ThrowHelper.ThrowApiException("Дружбу должен принять только Receiver ;)", ApiErrorCode.ValidationError);
    }
}