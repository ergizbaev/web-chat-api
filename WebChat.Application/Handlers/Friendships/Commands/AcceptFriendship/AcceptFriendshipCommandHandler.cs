﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using WebChat.Infrastructure.Databases.EntityFramework.Repositories.Interfaces;

namespace WebChat.Application.Handlers.Friendships.Commands.AcceptFriendship;

public class AcceptFriendshipCommandHandler : IRequestHandler<AcceptFriendshipCommandRequest, bool> {
    private readonly IFriendshipRepository _friendshipRepository;

    public AcceptFriendshipCommandHandler(IFriendshipRepository friendshipRepository) {
        this._friendshipRepository = friendshipRepository;
    }

    public async Task<bool> Handle(AcceptFriendshipCommandRequest request, CancellationToken cancellationToken) {
        var updatedCount = await this._friendshipRepository
            .GetAllRequests()
            .Where(x => x.Members.Any(m => m.Id == request.UserId))
            .ExecuteUpdateAsync(x =>
                    x.SetProperty(p => p.Accepted, true),
                cancellationToken);

        return updatedCount == 1;
    }
}