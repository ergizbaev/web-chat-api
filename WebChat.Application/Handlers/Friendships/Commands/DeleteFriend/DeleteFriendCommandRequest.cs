﻿using MediatR;

namespace WebChat.Application.Handlers.Friendships.Commands.DeleteFriend;

public class DeleteFriendCommandRequest : IRequest<bool> {
    public long UserId { get; set; }
}