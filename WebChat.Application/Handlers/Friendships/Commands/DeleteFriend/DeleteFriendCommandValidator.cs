﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using WebChat.Application.Common.Impl;
using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Shared.Helpers;
using WebChat.Infrastructure.Databases.EntityFramework.Repositories.Interfaces;
using WebChat.Infrastructure.Identity.Services.Interfaces;

namespace WebChat.Application.Handlers.Friendships.Commands.DeleteFriend;

public class DeleteFriendCommandValidator : RequestValidator<DeleteFriendCommandRequest> {
    private readonly IFriendshipRepository _friendshipRepository;

    public DeleteFriendCommandValidator(ICurrentUserService currentUserService, IFriendshipRepository friendshipRepository) {
        this._friendshipRepository = friendshipRepository;

        this.RuleFor(x => x.UserId)
            .NotEmpty().WithMessage("Заполните все поля")
            .Must(x => x != currentUserService.UserId);
    }

    public override async Task ValidateAsync(DeleteFriendCommandRequest request, CancellationToken cancellationToken) {
        var isFriends = await this._friendshipRepository.GetFriends().AnyAsync(x => x.Id == request.UserId, cancellationToken);
        if (!isFriends)
            ThrowHelper.ThrowApiException("Вы не являетесь друзьями", ApiErrorCode.ValidationError);
    }
}