﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using WebChat.Infrastructure.Databases.EntityFramework.Repositories.Interfaces;

namespace WebChat.Application.Handlers.Friendships.Commands.DeleteFriend;

public class DeleteFriendCommandHandler : IRequestHandler<DeleteFriendCommandRequest, bool> {
    private readonly IFriendshipRepository _friendshipRepository;

    public DeleteFriendCommandHandler(IFriendshipRepository friendshipRepository) {
        this._friendshipRepository = friendshipRepository;
    }

    public async Task<bool> Handle(DeleteFriendCommandRequest request, CancellationToken cancellationToken) {
        var deletedCount = await this._friendshipRepository
            .GetFriendship(request.UserId)
            .ExecuteDeleteAsync(cancellationToken);

        return deletedCount == 1;
    }
}