﻿using MediatR;
using WebChat.Application.DataContracts.Account.Responses;

namespace WebChat.Application.Handlers.Account.Commands.LoginUser;

public class LoginUserCommandRequest : IRequest<AuthResponse> {
    public string Email { get; set; }
    public string Password { get; set; }
}