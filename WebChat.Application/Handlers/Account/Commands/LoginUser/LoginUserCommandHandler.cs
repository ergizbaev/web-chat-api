﻿using System.Security.Claims;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebChat.Application.DataContracts.Account.Responses;
using WebChat.Infrastructure.Databases.EntityFramework;
using WebChat.Infrastructure.Identity.Services.Interfaces;

namespace WebChat.Application.Handlers.Account.Commands.LoginUser;

public class LoginUserCommandHandler : IRequestHandler<LoginUserCommandRequest, AuthResponse> {
    private readonly AppDbContext _dbContext;
    private readonly IJwtGeneratorService _jwtGeneratorService;

    public LoginUserCommandHandler(AppDbContext dbContext, IJwtGeneratorService jwtGeneratorService) {
        this._dbContext = dbContext;
        this._jwtGeneratorService = jwtGeneratorService;
    }

    public async Task<AuthResponse> Handle(LoginUserCommandRequest request, CancellationToken cancellationToken) {
        var user = await this._dbContext.Users
            .Where(x => EF.Functions.ILike(x.Email, $"{request.Email.Trim()}"))
            .Select(x => new { x.Id })
            .SingleAsync(cancellationToken);

        var claims = new Claim[] {
            new(ClaimsIdentity.DefaultNameClaimType, user.Id.ToString())
        };

        return new AuthResponse {
            Token = await this._jwtGeneratorService.GenerateJwt(claims)
        };
    }
}