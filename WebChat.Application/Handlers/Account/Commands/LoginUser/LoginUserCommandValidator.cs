﻿using FluentValidation;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using WebChat.Application.Common.Impl;
using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Entities.EntityFramework;
using WebChat.Domain.Shared.Helpers;
using WebChat.Infrastructure.Databases.EntityFramework;

namespace WebChat.Application.Handlers.Account.Commands.LoginUser;

public class LoginUserCommandValidator : RequestValidator<LoginUserCommandRequest> {
    private readonly AppDbContext _dbContext;
    private readonly IPasswordHasher<User> _passwordHasher;

    public LoginUserCommandValidator(AppDbContext dbContext, IPasswordHasher<User> passwordHasher) {
        this._dbContext = dbContext;
        this._passwordHasher = passwordHasher;

        this.RuleFor(x => x.Email).EmailAddress().WithMessage("Введите корректный Email");
        this.RuleFor(x => x.Password).NotEmpty().WithMessage("Введите пароль");
    }

    public override async Task ValidateAsync(LoginUserCommandRequest request, CancellationToken cancellationToken) {
        var user = await this._dbContext.Users
                        .Where(x => EF.Functions.ILike(x.Email, $"{request.Email.Trim()}"))
                        .Select(x => new { x.Password })
                        .SingleOrDefaultAsync(cancellationToken);
        if (user == null)
            ThrowHelper.ThrowApiException("Неверный логин или пароль V2", ApiErrorCode.ValidationError);

        var passwordVerifyResult = this._passwordHasher.VerifyHashedPassword(null, user.Password, request.Password);
        if (passwordVerifyResult != PasswordVerificationResult.Success)
            ThrowHelper.ThrowApiException("Неверный логин или пароль V2", ApiErrorCode.ValidationError);
    }
}