﻿using System.Security.Claims;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using WebChat.Application.DataContracts.Account.Responses;
using WebChat.Domain.Entities.EntityFramework;
using WebChat.Infrastructure.Databases.EntityFramework;
using WebChat.Infrastructure.Identity.Services.Interfaces;

namespace WebChat.Application.Handlers.Account.Commands.RegisterUser;

public class RegisterUserCommandHandler : IRequestHandler<RegisterUserCommandRequest, AuthResponse> {
    private readonly AppDbContext _dbContext;
    private readonly IJwtGeneratorService _jwtGeneratorService;
    private readonly IMapper _mapper;
    private readonly IPasswordHasher<User> _passwordHasher;

    public RegisterUserCommandHandler(IMapper mapper,
        AppDbContext dbContext,
        IPasswordHasher<User> passwordHasher,
        IJwtGeneratorService jwtGeneratorService) {
        this._mapper = mapper;
        this._dbContext = dbContext;
        this._passwordHasher = passwordHasher;
        this._jwtGeneratorService = jwtGeneratorService;
    }

    public async Task<AuthResponse> Handle(RegisterUserCommandRequest request, CancellationToken cancellationToken) {
        var user = this._mapper.Map<User>(request.User);
        user.Password = this._passwordHasher.HashPassword(user, request.User.Password);

        this._dbContext.Users.Add(user);
        await this._dbContext.SaveChangesAsync(cancellationToken);

        var claims = new Claim[] {
            new(ClaimsIdentity.DefaultNameClaimType, user.Id.ToString())
        };

        return new AuthResponse {
            Token = await this._jwtGeneratorService.GenerateJwt(claims)
        };
    }
}