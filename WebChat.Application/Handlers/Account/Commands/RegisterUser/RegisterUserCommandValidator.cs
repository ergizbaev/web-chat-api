﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using WebChat.Application.Common.Impl;
using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Shared.Helpers;
using WebChat.Infrastructure.Databases.EntityFramework;

namespace WebChat.Application.Handlers.Account.Commands.RegisterUser;

public class RegisterUserCommandValidator : RequestValidator<RegisterUserCommandRequest> {
    private readonly AppDbContext _dbContext;

    public RegisterUserCommandValidator(AppDbContext dbContext) {
        this._dbContext = dbContext;

        this.RuleFor(x => x.User.Email).EmailAddress().WithMessage("Введите корректный Email");
        this.RuleFor(x => x.User.FirstName).NotEmpty().WithMessage("Введите имя");
        this.RuleFor(x => x.User.Password).NotEmpty().WithMessage("Введите пароль");
    }

    public override async Task ValidateAsync(RegisterUserCommandRequest request, CancellationToken cancellationToken) {
        var separated = request.User.Email.Trim().Split(" ");
        if (separated.Length > 1) {
            ThrowHelper.ThrowApiException("Введите корректный Email", ApiErrorCode.ValidationError);
        }
        
        var userAlreadyExists = await this._dbContext.Users.AnyAsync(x => EF.Functions.ILike(x.Email, $"{request.User.Email.Trim()}"), cancellationToken);
        if (userAlreadyExists)
            ThrowHelper.ThrowApiException("Пользователь с данным Email уже зарегистрирован", ApiErrorCode.ValidationError);
    }
}