﻿using MediatR;
using WebChat.Application.DataContracts.Account;
using WebChat.Application.DataContracts.Account.Responses;

namespace WebChat.Application.Handlers.Account.Commands.RegisterUser;

public class RegisterUserCommandRequest : IRequest<AuthResponse> {
    public RegisterUserCommandRequest(in RegisterUserModel model) {
        this.User = model;
    }

    public RegisterUserModel User { get; }
}