﻿using MediatR;

namespace WebChat.Application.Handlers.Chats.Commands.EditMessage;

public class EditMessageCommandRequest : IRequest<bool> {
    public long MessageId { get; set; }
    public string NewText { get; set; }
}