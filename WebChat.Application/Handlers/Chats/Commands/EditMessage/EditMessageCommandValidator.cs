﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using WebChat.Application.Common.Impl;
using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Shared.Helpers;
using WebChat.Infrastructure.Databases.EntityFramework;
using WebChat.Infrastructure.Identity.Services.Interfaces;

namespace WebChat.Application.Handlers.Chats.Commands.EditMessage;

public class EditMessageCommandValidator : RequestValidator<EditMessageCommandRequest> {
    private readonly ICurrentUserService _currentUserService;
    private readonly AppDbContext _dbContext;

    public EditMessageCommandValidator(AppDbContext dbContext, ICurrentUserService currentUserService) {
        this._dbContext = dbContext;
        this._currentUserService = currentUserService;

        this.RuleFor(x => x.NewText).NotEmpty().WithMessage("Введите сообщение");
    }

    public override async Task ValidateAsync(EditMessageCommandRequest request, CancellationToken cancellationToken) {
        var messageExists = await this._dbContext.Messages
                                 .AnyAsync(x =>
                                     x.Id == request.MessageId &&
                                     x.Sender.Id == this._currentUserService.UserId, 
                                 cancellationToken);
        if (!messageExists)
            ThrowHelper.ThrowApiException("На найдено сообщение для редактирования", ApiErrorCode.ValidationError);
    }
}