﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using WebChat.Infrastructure.Databases.EntityFramework;

namespace WebChat.Application.Handlers.Chats.Commands.EditMessage;

public class EditMessageCommandHandler : IRequestHandler<EditMessageCommandRequest, bool> {
    private readonly AppDbContext _dbContext;

    public EditMessageCommandHandler(AppDbContext dbContext) {
        this._dbContext = dbContext;
    }

    public async Task<bool> Handle(EditMessageCommandRequest request, CancellationToken cancellationToken) {
        await this._dbContext.Messages
            .Where(x => x.Id == request.MessageId)
            .ExecuteUpdateAsync(x =>
                    x.SetProperty(p => p.Text, request.NewText)
                     .SetProperty(p => p.IsEdited, true)
                , cancellationToken);

        return true;
    }
}