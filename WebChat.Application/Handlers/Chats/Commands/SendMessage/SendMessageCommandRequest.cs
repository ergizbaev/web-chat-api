﻿using MediatR;
using Microsoft.AspNetCore.Http;

namespace WebChat.Application.Handlers.Chats.Commands.SendMessage;

public class SendMessageCommandRequest : IRequest<long> {
    public long? ChatId { get; set; }
    public long? ReceiverId { get; set; }
    public string Text { get; set; }
    public IFormFile[]? Files { get; set; }
}