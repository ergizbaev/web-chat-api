﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using WebChat.Application.Common.Impl;
using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Shared.Helpers;
using WebChat.Infrastructure.Databases.EntityFramework;
using WebChat.Infrastructure.Identity.Services.Interfaces;

namespace WebChat.Application.Handlers.Chats.Commands.SendMessage;

public class SendMessageCommandValidator : RequestValidator<SendMessageCommandRequest> {
    private readonly AppDbContext _dbContext;

    public SendMessageCommandValidator(AppDbContext dbContext, ICurrentUserService currentUserService) {
        this._dbContext = dbContext;

        this.RuleFor(x => x.ReceiverId).NotEmpty().WithMessage("Выберите собеседника");
        this.RuleFor(x => x.ReceiverId).Must(x => x != currentUserService.UserId);
        this.RuleFor(x => x.Text).NotEmpty().WithMessage("Введите сообщение");

        this.RuleFor(x => x.ChatId).NotEmpty().When(x => !x.ReceiverId.HasValue);
        this.RuleFor(x => x.ReceiverId).NotEmpty().When(x => !x.ChatId.HasValue);
    }

    public override async Task ValidateAsync(SendMessageCommandRequest request, CancellationToken cancellationToken) {
        if (request is { ReceiverId: { }, ChatId: { } })
            ThrowHelper.ThrowApiException("Некорректный запрос", ApiErrorCode.ValidationError);

        if (request.ReceiverId.HasValue) {
            var receiverHasExists = await this._dbContext.Users.AnyAsync(x => x.Id == request.ReceiverId, cancellationToken);
            if (!receiverHasExists)
                ThrowHelper.ThrowApiException("Собеседник не найден", ApiErrorCode.ValidationError);
        }

        if (request.ChatId.HasValue) {
            var chatHasExists = await this._dbContext.Chats.AnyAsync(x => x.Id == request.ChatId, cancellationToken);
            if (!chatHasExists)
                ThrowHelper.ThrowApiException("Чат не найден", ApiErrorCode.ValidationError);
        }
    }
}