﻿using System.Runtime.InteropServices;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Entities.EntityFramework;
using WebChat.Domain.Shared.Configs;
using WebChat.Domain.Shared.Extensions;
using WebChat.Infrastructure.Databases.EntityFramework;
using WebChat.Infrastructure.Identity.Services.Interfaces;
using WebChat.Infrastructure.SignalR.Hubs;

namespace WebChat.Application.Handlers.Chats.Commands.SendMessage;

public class SendMessageCommandHandler : IRequestHandler<SendMessageCommandRequest, long> {
    private readonly ICurrentUserExtendedService _currentUserService;
    private readonly AppDbContext _dbContext;
    private readonly IHubContext<ChatHub> _hubContext;

    public SendMessageCommandHandler(
        AppDbContext dbContext,
        ICurrentUserExtendedService currentUserService,
        IHubContext<ChatHub> hubContext
    ) {
        this._dbContext = dbContext;
        this._currentUserService = currentUserService;
        this._hubContext = hubContext;
    }

    public async Task<long> Handle(SendMessageCommandRequest request, CancellationToken cancellationToken) {
        var sender = await this._currentUserService.GetUserEntity(cancellationToken);

        var message = new Message(sender, request.Text);
        Chat chat = null!;

        if (request.ReceiverId.HasValue)
            chat = await this.GetOrCreatePrivateChat(sender, request.ReceiverId.Value, cancellationToken);
        else if (request.ChatId.HasValue)
            chat = await this._dbContext.Chats.SingleAsync(x => x.Id == request.ChatId, cancellationToken);

        if (request.Files != null && request.Files.Any())
            await SaveMessageFilesAsync(message, request.Files, cancellationToken);

        switch (chat.ChatType) {
            case ChatType.Private: {
                var receiverConnectionId = await this._dbContext.Users
                                                .Where(x => x.Id == request.ReceiverId.Value)
                                                .Select(x => x.ConnectionId)
                                                .SingleOrDefaultAsync(cancellationToken);
                if (receiverConnectionId.IsEmpty()) break;
                
                await this._hubContext.Clients.Client(receiverConnectionId).SendAsync("OnNewMessage", new { }, cancellationToken);
                break;
            }
            case ChatType.Group: {
                await this._hubContext.Clients.Group($"GroupChat-{chat.Id}").SendAsync("OnNewMessage", new { }, cancellationToken);
                break;
            }
        }

        chat.Messages.Add(message);
        this._dbContext.Chats.Update(chat);

        await this._dbContext.SaveChangesAsync(cancellationToken);

        return message.Id;
    }

    private async Task<Chat> GetOrCreatePrivateChat(User sender, long receiverId, CancellationToken cancellationToken = default) {
        var chat = await this._dbContext.Chats.SingleOrDefaultAsync(x =>
                x.ChatType == ChatType.Private &&
                x.Members.Any(u => u.Id == sender.Id) &&
                x.Members.Any(u => u.Id == receiverId),
            cancellationToken);

        if (chat != null)
            return chat;

        var receiver = await this._dbContext.Users.FindAsync(new object[] { receiverId }, cancellationToken);
        chat = new Chat(new[] { sender, receiver }!, ChatType.Private);
        this._dbContext.Chats.Add(chat);
        await this._dbContext.SaveChangesAsync(cancellationToken);

        return chat;
    }
    
    [DllImport("kernel32.dll", CharSet=CharSet.Auto, SetLastError=true)]
    public static extern bool DeleteFile(string path);

    private static async Task SaveMessageFilesAsync(Message message, IEnumerable<IFormFile> files, CancellationToken cancellationToken) {
        foreach (var file in files) {
            var fileUniqueName = string.Concat(Guid.NewGuid(), Path.GetExtension(file.FileName));
            var filePath = Path.Combine(FileStorageConfig.Path, fileUniqueName);

            using var ms = new MemoryStream();
            await file.CopyToAsync(ms, cancellationToken);
            await using Stream fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write);
            var bytes = ms.ToArray();
            await fileStream.WriteAsync(bytes, cancellationToken);

            message.Files.Add(new MessageFile(file.FileName, fileUniqueName));
        }
    }
}