﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using WebChat.Application.Common.Impl;
using WebChat.Domain.Constants;
using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Shared.Helpers;
using WebChat.Infrastructure.Databases.EntityFramework;
using WebChat.Infrastructure.Identity.Services.Interfaces;

namespace WebChat.Application.Handlers.Chats.Commands.InviteToGroupChat;

public class InviteToGroupChatCommandValidator : RequestValidator<InviteToGroupChatCommandRequest> {
    private readonly ICurrentUserService _currentUserService;
    private readonly AppDbContext _dbContext;

    public InviteToGroupChatCommandValidator(AppDbContext dbContext, ICurrentUserService currentUserService) {
        this._dbContext = dbContext;
        this._currentUserService = currentUserService;

        this.RuleFor(x => x.InvitedUsersIds).Must(x => x.Length > 0).WithMessage("Выберите хотя бы одного пользователя");
    }

    public override async Task ValidateAsync(InviteToGroupChatCommandRequest request, CancellationToken cancellationToken) {
        if (request.InvitedUsersIds.Any(x => x == this._currentUserService.UserId))
            ThrowHelper.ThrowApiException("Вы уже являетесь участником чата", ApiErrorCode.ValidationError);

        var existingInvitedUserCount = await this._dbContext.Users.CountAsync(x => request.InvitedUsersIds.Contains(x.Id), cancellationToken);
        if (existingInvitedUserCount != request.InvitedUsersIds.Length)
            ThrowHelper.ThrowApiException("Пользователь не найден", ApiErrorCode.ValidationError);

        var chat = await this._dbContext.Chats
            .Where(x =>
                x.Id == request.ChatId &&
                x.Members.Any(m => m.Id == this._currentUserService.UserId || request.InvitedUsersIds.Contains(x.Id)))
            .Select(x => new { MembersIds = x.Members.Select(m => m.Id) })
            .SingleOrDefaultAsync(cancellationToken);
        if (chat == null)
            ThrowHelper.ThrowApiException("Запрашивемый чат не найден", ApiErrorCode.ValidationError);

        if (chat.MembersIds.Count() + request.InvitedUsersIds.Length > ChatConstants.MaxMemberCount)
            ThrowHelper.ThrowApiException($"Количество участников не должно превышать {ChatConstants.MaxMemberCount}", ApiErrorCode.ValidationError);

        if (chat.MembersIds.All(x => x != this._currentUserService.UserId))
            ThrowHelper.ThrowApiException("Вы не являетесь участником чата", ApiErrorCode.ValidationError);

        if (request.InvitedUsersIds.Any(invitedUsersId => chat.MembersIds.Any(x => x == invitedUsersId)))
            ThrowHelper.ThrowApiException("Пользователь уже является участником чата", ApiErrorCode.ValidationError);
    }
}