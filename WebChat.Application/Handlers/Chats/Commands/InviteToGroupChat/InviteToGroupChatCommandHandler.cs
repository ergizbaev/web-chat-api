﻿using MassTransit;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebChat.Domain.Constants;
using WebChat.Domain.Events;
using WebChat.Infrastructure.Databases.EntityFramework;
using WebChat.Infrastructure.Identity.Services.Interfaces;

namespace WebChat.Application.Handlers.Chats.Commands.InviteToGroupChat;

public class InviteToGroupChatCommandHandler : IRequestHandler<InviteToGroupChatCommandRequest, bool> {
    private readonly ICurrentUserService _currentUserService;
    private readonly AppDbContext _dbContext;
    private readonly ISendEndpointProvider _sendEndpointProvider;

    public InviteToGroupChatCommandHandler(AppDbContext dbContext, ISendEndpointProvider sendEndpointProvider, ICurrentUserService currentUserService) {
        this._dbContext = dbContext;
        this._sendEndpointProvider = sendEndpointProvider;
        this._currentUserService = currentUserService;
    }

    public async Task<bool> Handle(InviteToGroupChatCommandRequest request, CancellationToken cancellationToken) {
        var chat = await this._dbContext.Chats.FindAsync(new object[] { request.ChatId }, cancellationToken);
        var invitedUsers = await this._dbContext.Users.Where(x => request.InvitedUsersIds.Contains(x.Id)).ToArrayAsync(cancellationToken);

        foreach (var invitedUser in invitedUsers)
            chat.Members.Add(invitedUser);

        this._dbContext.Chats.Update(chat);
        await this._dbContext.SaveChangesAsync(cancellationToken);

        var endpoint = await this._sendEndpointProvider.GetSendEndpoint(new Uri($"queue:{QueueNames.InviteUsersToGroup}"));
        await endpoint.Send(new UsersInvitedToGroupEvent {
            ChatId = chat.Id,
            InviterId = this._currentUserService.UserId,
            UsersIds = request.InvitedUsersIds
        }, cancellationToken);

        return true;
    }
}