﻿using System.ComponentModel.DataAnnotations;
using MediatR;

namespace WebChat.Application.Handlers.Chats.Commands.InviteToGroupChat;

public class InviteToGroupChatCommandRequest : IRequest<bool> {
    [Required]
    public long ChatId { get; set; }

    [Required]
    public long[] InvitedUsersIds { get; set; }
}