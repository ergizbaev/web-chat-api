﻿using Microsoft.EntityFrameworkCore;
using WebChat.Application.Common.Impl;
using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Shared.Helpers;
using WebChat.Infrastructure.Databases.EntityFramework;
using WebChat.Infrastructure.Identity.Services.Interfaces;

namespace WebChat.Application.Handlers.Chats.Commands.DeleteMessage; 

public class DeleteMessageCommandValidator : RequestValidator<DeleteMessageCommandRequest> {
    private readonly AppDbContext _dbContext;
    private readonly ICurrentUserService _currentUserService;

    public DeleteMessageCommandValidator(AppDbContext dbContext, ICurrentUserService currentUserService) {
        this._dbContext = dbContext;
        this._currentUserService = currentUserService;
    }

    public override async Task ValidateAsync(DeleteMessageCommandRequest request, CancellationToken cancellationToken) {
        var messageExists = await this._dbContext.Messages
                               .AnyAsync(x =>
                                   x.Id == request.MessageId &&
                                   x.Sender.Id == this._currentUserService.UserId, 
                               cancellationToken);
        if (!messageExists)
            ThrowHelper.ThrowApiException("На найдено сообщение для удаления", ApiErrorCode.ValidationError);
    }
}