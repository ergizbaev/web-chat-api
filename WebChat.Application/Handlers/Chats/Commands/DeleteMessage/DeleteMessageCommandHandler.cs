﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using WebChat.Infrastructure.Databases.EntityFramework;

namespace WebChat.Application.Handlers.Chats.Commands.DeleteMessage;

public class DeleteMessageCommandHandler : IRequestHandler<DeleteMessageCommandRequest, bool> {
    private readonly AppDbContext _dbContext;

    public DeleteMessageCommandHandler(AppDbContext dbContext) {
        this._dbContext = dbContext;
    }

    public async Task<bool> Handle(DeleteMessageCommandRequest request, CancellationToken cancellationToken) {
        await this._dbContext.Messages
            .Where(x => x.Id == request.MessageId)
            .ExecuteDeleteAsync(cancellationToken);
        return true;
    }
}