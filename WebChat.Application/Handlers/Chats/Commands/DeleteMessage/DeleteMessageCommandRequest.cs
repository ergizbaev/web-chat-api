﻿using MediatR;

namespace WebChat.Application.Handlers.Chats.Commands.DeleteMessage; 

public class DeleteMessageCommandRequest : IRequest<bool> {
    public long MessageId { get; set; }
}