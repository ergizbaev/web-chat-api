﻿using MassTransit;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebChat.Domain.Constants;
using WebChat.Domain.Entities.EntityFramework;
using WebChat.Domain.Events;
using WebChat.Infrastructure.Databases.EntityFramework;
using WebChat.Infrastructure.Identity.Services.Interfaces;

namespace WebChat.Application.Handlers.Chats.Commands.CreateGroupChat;

public class CreateGroupChatCommandRequestHandler : IRequestHandler<CreateGroupChatCommandRequest, long> {
    private readonly ICurrentUserService _currentUserService;
    private readonly AppDbContext _dbContext;
    private readonly ISendEndpointProvider _sendEndpointProvider;

    public CreateGroupChatCommandRequestHandler(AppDbContext dbContext, ICurrentUserService currentUserService, ISendEndpointProvider sendEndpointProvider) {
        this._dbContext = dbContext;
        this._currentUserService = currentUserService;
        this._sendEndpointProvider = sendEndpointProvider;
    }

    public async Task<long> Handle(CreateGroupChatCommandRequest request, CancellationToken cancellationToken) {
        request.Members.Add(this._currentUserService.UserId);
        
        var requestedMembers = await this._dbContext.Users
                                    .Where(x => request.Members.Contains(x.Id))
                                    .ToArrayAsync(cancellationToken);

        var chat = new Chat(request.Title, requestedMembers);

        this._dbContext.Chats.Add(chat);
        await this._dbContext.SaveChangesAsync(cancellationToken);

        var endpoint = await this._sendEndpointProvider.GetSendEndpoint(new Uri($"queue:{QueueNames.InviteUsersToGroup}"));
        await endpoint.Send(new UsersInvitedToGroupEvent {
            ChatId = chat.Id,
            InviterId = this._currentUserService.UserId,
            UsersIds = requestedMembers.Where(x => x.Id != this._currentUserService.UserId).Select(x => x.Id).ToArray()
        }, cancellationToken);

        return chat.Id;
    }
}