﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using WebChat.Application.Common.Impl;
using WebChat.Domain.Constants;
using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Shared.Helpers;
using WebChat.Infrastructure.Databases.EntityFramework;
using WebChat.Infrastructure.Identity.Services.Interfaces;

namespace WebChat.Application.Handlers.Chats.Commands.CreateGroupChat;

public class CreateGroupChatCommandRequestValidator : RequestValidator<CreateGroupChatCommandRequest> {
    private readonly ICurrentUserService _currentUserService;
    private readonly AppDbContext _dbContext;

    public CreateGroupChatCommandRequestValidator(AppDbContext dbContext, ICurrentUserService currentUserService) {
        this._dbContext = dbContext;
        this._currentUserService = currentUserService;

        this.RuleLevelCascadeMode = CascadeMode.Stop;

        this.RuleFor(x => x.Title).NotEmpty().WithMessage("Введите наименование группового чата");
        
        this.RuleFor(x => x.Members).Must(x => x is { Count : <= ChatConstants.MaxMemberCount })
            .WithMessage($"Количество участников не должно превышать {ChatConstants.MaxMemberCount}");

        this.RuleFor(x => x.Members).Must(x => x.Distinct().Count() == x.Count)
            .WithMessage($"Количество участников не должно превышать {ChatConstants.MaxMemberCount}");
    }

    public override async Task ValidateAsync(CreateGroupChatCommandRequest request, CancellationToken cancellationToken) {
        var currentUserId = this._currentUserService.UserId;
        if (request.Members.Any(x => x == currentUserId))
            ThrowHelper.ThrowApiException("Выберите участник чата корректно", ApiErrorCode.ValidationError);

        var requestedMembersCount = await this._dbContext.Users.CountAsync(x => request.Members.Contains(x.Id), cancellationToken);
        if (requestedMembersCount != request.Members.Count)
            ThrowHelper.ThrowApiException("Ошибка запроса", ApiErrorCode.ValidationError);
    }
}