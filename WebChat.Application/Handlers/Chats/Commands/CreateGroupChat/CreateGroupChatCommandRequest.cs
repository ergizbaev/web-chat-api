﻿using System.ComponentModel.DataAnnotations;
using MediatR;

namespace WebChat.Application.Handlers.Chats.Commands.CreateGroupChat;

public class CreateGroupChatCommandRequest : IRequest<long> {
    public string Title { get; set; }

    [Required(ErrorMessage = "Необходимо выбрать собеседника (-ов)")]
    public List<long> Members { get; set; }
}