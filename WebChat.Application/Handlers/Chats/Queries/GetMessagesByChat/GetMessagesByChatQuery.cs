﻿using MediatR;
using WebChat.Application.DataContracts.Messages;

namespace WebChat.Application.Handlers.Chats.Queries.GetMessagesByChat;

public class GetMessagesByChatQuery : IRequest<MessageDto[]> {
    public GetMessagesByChatQuery(in long chatId) {
        this.ChatId = chatId;
    }

    public long ChatId { get; }
}