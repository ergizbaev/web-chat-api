﻿using Microsoft.EntityFrameworkCore;
using WebChat.Application.Common.Impl;
using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Shared.Helpers;
using WebChat.Infrastructure.Databases.EntityFramework;
using WebChat.Infrastructure.Identity.Services.Interfaces;

namespace WebChat.Application.Handlers.Chats.Queries.GetMessagesByChat;

public class GetMessagesByChatQueryValidator : RequestValidator<GetMessagesByChatQuery> {
    private readonly ICurrentUserService _currentUserService;
    private readonly AppDbContext _dbContext;

    public GetMessagesByChatQueryValidator(AppDbContext dbContext, ICurrentUserService currentUserService) {
        this._dbContext = dbContext;
        this._currentUserService = currentUserService;
    }

    public override async Task ValidateAsync(GetMessagesByChatQuery request, CancellationToken cancellationToken) {
        var chat = await this._dbContext.Chats
            .Where(x => x.Id == request.ChatId)
            .Select(x => new { MembersIds = x.Members.Where(m => m.Id == this._currentUserService.UserId).Select(m => m.Id) })
            .SingleOrDefaultAsync(cancellationToken);
        if (chat == null)
            ThrowHelper.ThrowApiException("Запрашиваемый чат не найден", ApiErrorCode.ValidationError);

        if (!chat.MembersIds.Any())
            ThrowHelper.ThrowApiException("У вас нет доступа к данному чату", ApiErrorCode.ValidationError);
    }
}