﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebChat.Application.DataContracts.Messages;
using WebChat.Infrastructure.Databases.EntityFramework;

namespace WebChat.Application.Handlers.Chats.Queries.GetMessagesByChat;

public class GetMessagesByChatQueryHandler : IRequestHandler<GetMessagesByChatQuery, MessageDto[]> {
    private readonly AppDbContext _dbContext;
    private readonly IMapper _mapper;

    public GetMessagesByChatQueryHandler(IMapper mapper, AppDbContext dbContext) {
        this._mapper = mapper;
        this._dbContext = dbContext;
    }

    public async Task<MessageDto[]> Handle(GetMessagesByChatQuery request, CancellationToken cancellationToken) {
        return await this._dbContext.Chats
            .Where(x => x.Id == request.ChatId)
            .SelectMany(x => x.Messages)
            .ProjectTo<MessageDto>(this._mapper.ConfigurationProvider)
            .ToArrayAsync(cancellationToken);
    }
}