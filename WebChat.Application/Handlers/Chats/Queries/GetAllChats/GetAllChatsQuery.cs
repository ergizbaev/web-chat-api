﻿using MediatR;
using WebChat.Application.DataContracts.Chats;

namespace WebChat.Application.Handlers.Chats.Queries.GetAllChats;

public class GetAllChatsQuery : IRequest<IQueryable<ChatDto>> {
}