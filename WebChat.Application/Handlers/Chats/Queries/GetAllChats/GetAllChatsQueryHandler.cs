﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using WebChat.Application.DataContracts.Chats;
using WebChat.Infrastructure.Databases.EntityFramework;
using WebChat.Infrastructure.Identity.Services.Interfaces;

namespace WebChat.Application.Handlers.Chats.Queries.GetAllChats;

public class GetAllChatsQueryHandler : IRequestHandler<GetAllChatsQuery, IQueryable<ChatDto>> {
    private readonly ICurrentUserService _currentUserService;
    private readonly AppDbContext _dbContext;
    private readonly IMapper _mapper;

    public GetAllChatsQueryHandler(IMapper mapper, AppDbContext dbContext, ICurrentUserService currentUserService) {
        this._mapper = mapper;
        this._dbContext = dbContext;
        this._currentUserService = currentUserService;
    }

    public async Task<IQueryable<ChatDto>> Handle(GetAllChatsQuery request, CancellationToken cancellationToken) {
        var currentUserId = this._currentUserService.UserId;
        return await Task.FromResult(
            this._dbContext.Users
                .Where(x => x.Id == currentUserId)
                .SelectMany(x => x.Chats)
                .ProjectTo<ChatDto>(this._mapper.ConfigurationProvider));
    }
}