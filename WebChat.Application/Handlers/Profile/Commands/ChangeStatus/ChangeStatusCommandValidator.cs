﻿using FluentValidation;
using WebChat.Application.Common.Impl;

namespace WebChat.Application.Handlers.Profile.Commands.ChangeStatus;

public class ChangeStatusCommandValidator : RequestValidator<ChangeStatusCommandRequest> {
    public ChangeStatusCommandValidator() {
        this.RuleFor(x => x.Status).IsInEnum();
    }
}