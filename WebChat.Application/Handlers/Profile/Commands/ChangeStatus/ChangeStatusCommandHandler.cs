﻿using MediatR;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using WebChat.Domain.Constants.Enum;
using WebChat.Infrastructure.Databases.EntityFramework;
using WebChat.Infrastructure.Databases.EntityFramework.Repositories.Interfaces;
using WebChat.Infrastructure.Identity.Services.Interfaces;
using WebChat.Infrastructure.SignalR.Hubs;

namespace WebChat.Application.Handlers.Profile.Commands.ChangeStatus;

public class ChangeStatusCommandHandler : IRequestHandler<ChangeStatusCommandRequest, bool> {
    private readonly ICurrentUserService _currentUserService;
    private readonly AppDbContext _dbContext;
    private readonly IFriendshipRepository _friendshipRepository;
    private readonly IHubContext<ChatHub> _hubContext;

    public ChangeStatusCommandHandler(AppDbContext dbContext, IHubContext<ChatHub> hubContext, ICurrentUserService currentUserService, IFriendshipRepository friendshipRepository) {
        this._dbContext = dbContext;
        this._hubContext = hubContext;
        this._currentUserService = currentUserService;
        this._friendshipRepository = friendshipRepository;
    }

    public async Task<bool> Handle(ChangeStatusCommandRequest request, CancellationToken cancellationToken) {
        var userId = this._currentUserService.UserId;
        await this._dbContext.Users
            .Where(x => x.Id == userId)
            .ExecuteUpdateAsync(x =>
                    x.SetProperty(p => p.Status, request.Status),
                cancellationToken);

        var friendsConnections = await this._friendshipRepository
            .GetFriends(x => x.Status != UserStatusEnum.Offline)
            .Select(x => x.ConnectionId)
            .ToArrayAsync(cancellationToken);

        this._hubContext.Clients.Clients(friendsConnections).SendAsync("OnUserStatusChange", new {
            UserId = userId, request.Status
        }, cancellationToken);

        return true;
    }
}