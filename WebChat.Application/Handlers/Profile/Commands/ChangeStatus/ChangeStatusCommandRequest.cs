﻿using MediatR;
using WebChat.Domain.Constants.Enum;

namespace WebChat.Application.Handlers.Profile.Commands.ChangeStatus;

public class ChangeStatusCommandRequest : IRequest<bool> {
    public UserStatusEnum Status { get; set; }
}