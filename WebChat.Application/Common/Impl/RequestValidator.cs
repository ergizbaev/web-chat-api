﻿using FluentValidation;
using WebChat.Application.Common.Interfaces;
using WebChat.Domain.Constants.Enum;
using WebChat.Domain.Shared.Helpers;

namespace WebChat.Application.Common.Impl;

public abstract class RequestValidator<T> : AbstractValidator<T>, IRequestValidator<T> {
    public new virtual Task ValidateAsync(T request, CancellationToken cancellationToken) {
        return Task.CompletedTask;
    }

    public async Task ValidateFluentRulesAsync(T request, CancellationToken cancellationToken) {
        var result = await base.ValidateAsync(request, cancellationToken);
        if (!result.IsValid) {
            var errors = result.Errors.Select(x => x.ErrorMessage);
            ThrowHelper.ThrowApiException(string.Join(Environment.NewLine, errors), ApiErrorCode.ValidationError);
        }
    }
}