﻿namespace WebChat.Application.Common.Interfaces;

public interface IRequestValidator<in TRequest> {
    Task ValidateAsync(TRequest request, CancellationToken cancellationToken);
    Task ValidateFluentRulesAsync(TRequest request, CancellationToken cancellationToken);
}