﻿using MediatR;
using WebChat.Application.Common.Interfaces;

namespace WebChat.Application.Common.Behaviours;

public class RequestValidatorBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest : IRequest<TResponse> {
    private readonly IEnumerable<IRequestValidator<TRequest>> _validators;

    public RequestValidatorBehaviour(IEnumerable<IRequestValidator<TRequest>> validators) {
        this._validators = validators;
    }

    public async Task<TResponse> Handle(TRequest request, RequestHandlerDelegate<TResponse> next, CancellationToken cancellationToken) {
        foreach (var validator in this._validators) {
            await validator.ValidateFluentRulesAsync(request, cancellationToken);
            await validator.ValidateAsync(request, cancellationToken);
        }

        return await next();
    }
}