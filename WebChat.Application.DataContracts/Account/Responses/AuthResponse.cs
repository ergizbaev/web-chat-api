﻿namespace WebChat.Application.DataContracts.Account.Responses;

public class AuthResponse {
    public string Token { get; set; }
}