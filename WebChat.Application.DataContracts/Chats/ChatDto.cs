﻿using WebChat.Domain.Constants.Enum;

namespace WebChat.Application.DataContracts.Chats;

public class ChatDto {
    public long Id { get; set; }
    public string? Title { get; set; }
    public string? LastMessage { get; set; }
    public ChatType ChatType { get; set; }
    public DateTimeOffset CreationDate { get; set; }
}