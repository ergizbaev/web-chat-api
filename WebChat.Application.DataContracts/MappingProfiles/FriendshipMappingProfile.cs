﻿using AutoMapper;
using WebChat.Application.DataContracts.Friendships;
using WebChat.Domain.Entities.EntityFramework;

namespace WebChat.Application.DataContracts.MappingProfiles;

public class FriendshipMappingProfile : Profile {
    public FriendshipMappingProfile() {
        this.CreateMap<User, FriendDto>()
            .ForMember(d => d.FullName, m => m.MapFrom(s => s.FirstName + " " + s.LastName));

        this.CreateMap<User, FriendshipRequestUserDto>()
            .ForMember(d => d.FullName, m => m.MapFrom(s => s.FirstName + " " + s.LastName));
    }
}