﻿using AutoMapper;
using WebChat.Application.DataContracts.Account;
using WebChat.Domain.Entities.EntityFramework;

namespace WebChat.Application.DataContracts.MappingProfiles;

public class AccountMappingProfile : Profile {
    public AccountMappingProfile() {
        this.CreateMap<RegisterUserModel, User>()
            .ForMember(d => d.Email, opt => opt.MapFrom(s => s.Email.Trim()))
            .ForMember(d => d.Password, opt => opt.Ignore());
    }
}