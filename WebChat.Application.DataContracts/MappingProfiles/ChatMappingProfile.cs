﻿using AutoMapper;
using WebChat.Application.DataContracts.Chats;
using WebChat.Application.DataContracts.Messages;
using WebChat.Domain.Entities.EntityFramework;

namespace WebChat.Application.DataContracts.MappingProfiles;

public class ChatMappingProfile : Profile {
    public ChatMappingProfile() {
        this.CreateMap<Chat, ChatDto>();

        this.CreateMap<Message, MessageDto>();
    }
}