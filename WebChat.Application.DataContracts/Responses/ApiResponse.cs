﻿using System.Text.Json.Serialization;
using WebChat.Domain.Constants.Enum;

namespace WebChat.Application.DataContracts.Responses; 

public class ApiResponse {
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
    public ApiError? Error { get; set; }

    private ApiResponse(in ApiError error) {
        this.Error = error;
    }
    
    protected ApiResponse() { }

    public static ApiResponse<T> Success<T>(in T data) => new(data);
    public static ApiResponse Failed(in ApiError error) => new(error);
}

public class ApiResponse<T> : ApiResponse {
    public T Data { get; set; }
    
    public ApiResponse(T data) {
        Data = data;
    }
}

public class ApiError {
    public string Text { get; set; }
    public ApiErrorCode Code { get; set; }

    public ApiError(string text, ApiErrorCode code) {
        this.Text = text;
        this.Code = code;
    }
}