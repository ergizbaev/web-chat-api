﻿using WebChat.Domain.Constants.Enum;

namespace WebChat.Application.DataContracts.Friendships;

public class FriendDto {
    public long Id { get; set; }
    public string FullName { get; set; }
    public UserStatusEnum Status { get; set; }
}