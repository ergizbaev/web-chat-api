﻿using WebChat.Domain.Constants.Enum;

namespace WebChat.Application.DataContracts.Friendships;

public class FriendshipRequestUserDto {
    public long Id { get; set; }
    public string Photo { get; set; }
    public string Email { get; set; }
    public string FullName { get; set; }
    public UserStatusEnum Status { get; set; }
    public DateTimeOffset? LastOnlineDate { get; set; }
}