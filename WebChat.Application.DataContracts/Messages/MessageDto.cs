﻿namespace WebChat.Application.DataContracts.Messages;

public class MessageDto {
    public long Id { get; set; }
    public string Text { get; set; }
    public bool IsEdited { get; set; }
    public DateTimeOffset SentDate { get; set; }
}